/**
 */
package at.ac.tuwien.ame.petrinet;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Transition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.ame.petrinet.Transition#getSources <em>Sources</em>}</li>
 *   <li>{@link at.ac.tuwien.ame.petrinet.Transition#getTargets <em>Targets</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.ame.petrinet.PetrinetPackage#getTransition()
 * @model
 * @generated
 */
public interface Transition extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Sources</b></em>' reference list.
	 * The list contents are of type {@link at.ac.tuwien.ame.petrinet.Place}.
	 * It is bidirectional and its opposite is '{@link at.ac.tuwien.ame.petrinet.Place#getOutgoingTransitions <em>Outgoing Transitions</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sources</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sources</em>' reference list.
	 * @see at.ac.tuwien.ame.petrinet.PetrinetPackage#getTransition_Sources()
	 * @see at.ac.tuwien.ame.petrinet.Place#getOutgoingTransitions
	 * @model opposite="outgoingTransitions"
	 * @generated
	 */
	EList<Place> getSources();

	/**
	 * Returns the value of the '<em><b>Targets</b></em>' reference list.
	 * The list contents are of type {@link at.ac.tuwien.ame.petrinet.Place}.
	 * It is bidirectional and its opposite is '{@link at.ac.tuwien.ame.petrinet.Place#getIncomingTransitions <em>Incoming Transitions</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Targets</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Targets</em>' reference list.
	 * @see at.ac.tuwien.ame.petrinet.PetrinetPackage#getTransition_Targets()
	 * @see at.ac.tuwien.ame.petrinet.Place#getIncomingTransitions
	 * @model opposite="incomingTransitions"
	 * @generated
	 */
	EList<Place> getTargets();

} // Transition
