package at.ac.tuwien.me.xspetrinet;

import at.ac.tuwien.me.xspetrinet.XSPetriNetMT;
import fr.inria.diverse.melange.lib.IMetamodel;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;

@SuppressWarnings("all")
public class XSPetriNet implements IMetamodel {
  private Resource resource;
  
  public Resource getResource() {
    return this.resource;
  }
  
  public void setResource(final Resource resource) {
    this.resource = resource;
  }
  
  public static XSPetriNet load(final String uri) {
    ResourceSet rs = new ResourceSetImpl();
    Resource res = rs.getResource(URI.createURI(uri), true);
    XSPetriNet mm = new XSPetriNet();
    mm.setResource(res);
    return mm ;
  }
  
  public XSPetriNetMT toXSPetriNetMT() {
    at.ac.tuwien.me.xspetrinet.xspetrinet.adapters.xspetrinetmt.XSPetriNetAdapter adaptee = new at.ac.tuwien.me.xspetrinet.xspetrinet.adapters.xspetrinetmt.XSPetriNetAdapter() ;
    adaptee.setAdaptee(resource);
    return adaptee;
  }
}
