package at.ac.tuwien.me.xspetrinet.xspetrinet.adapters.xspetrinetmt;

import at.ac.tuwien.me.xspetrinet.XSPetriNetMT;
import at.ac.tuwien.me.xspetrinet.xspetrinetmt.petrinet.PetrinetFactory;
import fr.inria.diverse.melange.adapters.ResourceAdapter;
import java.io.IOException;
import java.util.Set;
import org.eclipse.emf.common.util.URI;

@SuppressWarnings("all")
public class XSPetriNetAdapter extends ResourceAdapter implements XSPetriNetMT {
  public XSPetriNetAdapter() {
    super(at.ac.tuwien.me.xspetrinet.xspetrinet.adapters.xspetrinetmt.XSPetriNetMTAdaptersFactory.getInstance());
  }
  
  @Override
  public PetrinetFactory getPetrinetFactory() {
    return new at.ac.tuwien.me.xspetrinet.xspetrinet.adapters.xspetrinetmt.petrinet.PetrinetFactoryAdapter();
  }
  
  @Override
  public Set getFactories() {
    java.util.Set<org.eclipse.emf.ecore.EFactory> res = new java.util.HashSet<org.eclipse.emf.ecore.EFactory>();
    res.add(getPetrinetFactory());
    return res;
  }
  
  @Override
  public void save(final String uri) throws IOException {
    this.adaptee.setURI(URI.createURI(uri));
    this.adaptee.save(null);
  }
}
