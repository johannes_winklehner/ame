package at.ac.tuwien.me.xspetrinet.xspetrinet.adapters.xspetrinetmt.petrinet;

import at.ac.tuwien.me.xspetrinet.xspetrinet.adapters.xspetrinetmt.XSPetriNetMTAdaptersFactory;
import at.ac.tuwien.me.xspetrinet.xspetrinet.petrinet.Net;
import at.ac.tuwien.me.xspetrinet.xspetrinetmt.petrinet.Place;
import at.ac.tuwien.me.xspetrinet.xspetrinetmt.petrinet.Transition;
import fr.inria.diverse.melange.adapters.EObjectAdapter;
import java.util.Collection;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;

@SuppressWarnings("all")
public class NetAdapter extends EObjectAdapter<Net> implements at.ac.tuwien.me.xspetrinet.xspetrinetmt.petrinet.Net {
  private XSPetriNetMTAdaptersFactory adaptersFactory;
  
  public NetAdapter() {
    super(at.ac.tuwien.me.xspetrinet.xspetrinet.adapters.xspetrinetmt.XSPetriNetMTAdaptersFactory.getInstance());
    adaptersFactory = at.ac.tuwien.me.xspetrinet.xspetrinet.adapters.xspetrinetmt.XSPetriNetMTAdaptersFactory.getInstance();
  }
  
  @Override
  public String getName() {
    return adaptee.getName();
  }
  
  @Override
  public void setName(final String o) {
    adaptee.setName(o);
  }
  
  private EList<Place> places_;
  
  @Override
  public EList<Place> getPlaces() {
    if (places_ == null)
    	places_ = fr.inria.diverse.melange.adapters.EListAdapter.newInstance(adaptee.getPlaces(), adaptersFactory, eResource);
    return places_;
  }
  
  private EList<Transition> transitions_;
  
  @Override
  public EList<Transition> getTransitions() {
    if (transitions_ == null)
    	transitions_ = fr.inria.diverse.melange.adapters.EListAdapter.newInstance(adaptee.getTransitions(), adaptersFactory, eResource);
    return transitions_;
  }
  
  @Override
  public void main() {
    at.ac.tuwien.me.xspetrinet.xspetrinet.aspects.NetAspect.main(adaptee);
  }
  
  protected final static String NAME_EDEFAULT = null;
  
  @Override
  public EClass eClass() {
    return at.ac.tuwien.me.xspetrinet.xspetrinetmt.petrinet.PetrinetPackage.eINSTANCE.getNet();
  }
  
  @Override
  public Object eGet(final int featureID, final boolean resolve, final boolean coreType) {
    switch (featureID) {
    	case at.ac.tuwien.me.xspetrinet.xspetrinetmt.petrinet.PetrinetPackage.NET__NAME:
    		return getName();
    	case at.ac.tuwien.me.xspetrinet.xspetrinetmt.petrinet.PetrinetPackage.NET__PLACES:
    		return getPlaces();
    	case at.ac.tuwien.me.xspetrinet.xspetrinetmt.petrinet.PetrinetPackage.NET__TRANSITIONS:
    		return getTransitions();
    }
    
    return super.eGet(featureID, resolve, coreType);
  }
  
  @Override
  public boolean eIsSet(final int featureID) {
    switch (featureID) {
    	case at.ac.tuwien.me.xspetrinet.xspetrinetmt.petrinet.PetrinetPackage.NET__NAME:
    		return getName() != NAME_EDEFAULT;
    	case at.ac.tuwien.me.xspetrinet.xspetrinetmt.petrinet.PetrinetPackage.NET__PLACES:
    		return getPlaces() != null && !getPlaces().isEmpty();
    	case at.ac.tuwien.me.xspetrinet.xspetrinetmt.petrinet.PetrinetPackage.NET__TRANSITIONS:
    		return getTransitions() != null && !getTransitions().isEmpty();
    }
    
    return super.eIsSet(featureID);
  }
  
  @Override
  public void eSet(final int featureID, final Object newValue) {
    switch (featureID) {
    	case at.ac.tuwien.me.xspetrinet.xspetrinetmt.petrinet.PetrinetPackage.NET__NAME:
    		setName(
    		(java.lang.String)
    		 newValue);
    		return;
    	case at.ac.tuwien.me.xspetrinet.xspetrinetmt.petrinet.PetrinetPackage.NET__PLACES:
    		getPlaces().clear();
    		getPlaces().addAll((Collection) newValue);
    		return;
    	case at.ac.tuwien.me.xspetrinet.xspetrinetmt.petrinet.PetrinetPackage.NET__TRANSITIONS:
    		getTransitions().clear();
    		getTransitions().addAll((Collection) newValue);
    		return;
    }
    
    super.eSet(featureID, newValue);
  }
}
