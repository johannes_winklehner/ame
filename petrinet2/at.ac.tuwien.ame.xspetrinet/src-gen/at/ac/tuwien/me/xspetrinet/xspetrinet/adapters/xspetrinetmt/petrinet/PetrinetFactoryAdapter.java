package at.ac.tuwien.me.xspetrinet.xspetrinet.adapters.xspetrinetmt.petrinet;

import at.ac.tuwien.me.xspetrinet.xspetrinet.adapters.xspetrinetmt.XSPetriNetMTAdaptersFactory;
import at.ac.tuwien.me.xspetrinet.xspetrinetmt.petrinet.Net;
import at.ac.tuwien.me.xspetrinet.xspetrinetmt.petrinet.PetrinetFactory;
import at.ac.tuwien.me.xspetrinet.xspetrinetmt.petrinet.PetrinetPackage;
import at.ac.tuwien.me.xspetrinet.xspetrinetmt.petrinet.Place;
import at.ac.tuwien.me.xspetrinet.xspetrinetmt.petrinet.Transition;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;

@SuppressWarnings("all")
public class PetrinetFactoryAdapter extends EFactoryImpl implements PetrinetFactory {
  private XSPetriNetMTAdaptersFactory adaptersFactory = at.ac.tuwien.me.xspetrinet.xspetrinet.adapters.xspetrinetmt.XSPetriNetMTAdaptersFactory.getInstance();
  
  private at.ac.tuwien.me.xspetrinet.xspetrinet.petrinet.PetrinetFactory petrinetAdaptee = at.ac.tuwien.me.xspetrinet.xspetrinet.petrinet.PetrinetFactory.eINSTANCE;
  
  @Override
  public Net createNet() {
    return adaptersFactory.createNetAdapter(petrinetAdaptee.createNet(), null);
  }
  
  @Override
  public Place createPlace() {
    return adaptersFactory.createPlaceAdapter(petrinetAdaptee.createPlace(), null);
  }
  
  @Override
  public Transition createTransition() {
    return adaptersFactory.createTransitionAdapter(petrinetAdaptee.createTransition(), null);
  }
  
  @Override
  public EPackage getEPackage() {
    return getPetrinetPackage();
  }
  
  public PetrinetPackage getPetrinetPackage() {
    return at.ac.tuwien.me.xspetrinet.xspetrinetmt.petrinet.PetrinetPackage.eINSTANCE;
  }
}
