package at.ac.tuwien.me.xspetrinet;

import fr.inria.diverse.melange.resource.MelangeRegistry;
import fr.inria.diverse.melange.resource.MelangeRegistryImpl;
import fr.inria.diverse.melange.resource.MelangeResourceFactoryImpl;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

@SuppressWarnings("all")
public class StandaloneSetup {
  public static void doSetup() {
    StandaloneSetup setup = new StandaloneSetup();
    setup.doEMFRegistration();
    setup.doAdaptersRegistration();
  }
  
  public void doEMFRegistration() {
    EPackage.Registry.INSTANCE.put(
    	at.ac.tuwien.me.xspetrinet.xspetrinet.petrinet.PetrinetPackage.eNS_URI,
    	at.ac.tuwien.me.xspetrinet.xspetrinet.petrinet.PetrinetPackage.eINSTANCE
    );
    
    Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put(
    	"*",
    	new XMIResourceFactoryImpl()
    );
    Resource.Factory.Registry.INSTANCE.getProtocolToFactoryMap().put(
    	"melange",
    	new MelangeResourceFactoryImpl()
    );
  }
  
  public void doAdaptersRegistration() {
    MelangeRegistry.LanguageDescriptor xSPetriNet = new MelangeRegistryImpl.LanguageDescriptorImpl(
    	"at.ac.tuwien.me.xspetrinet.XSPetriNet", "", "http://at.ac.tuwien.me.xspetrinet.xspetrinet/petrinet/", "at.ac.tuwien.me.xspetrinet.XSPetriNetMT"
    );
    xSPetriNet.addAdapter("at.ac.tuwien.me.xspetrinet.XSPetriNetMT", at.ac.tuwien.me.xspetrinet.xspetrinet.adapters.xspetrinetmt.XSPetriNetAdapter.class);
    MelangeRegistry.INSTANCE.getLanguageMap().put(
    	"at.ac.tuwien.me.xspetrinet.XSPetriNet",
    	xSPetriNet
    );
    MelangeRegistry.ModelTypeDescriptor xSPetriNetMT = new MelangeRegistryImpl.ModelTypeDescriptorImpl(
    	"at.ac.tuwien.me.xspetrinet.XSPetriNetMT", "", "http://at.ac.tuwien.me.xspetrinet.xspetrinetmt/"
    );
    MelangeRegistry.INSTANCE.getModelTypeMap().put(
    	"at.ac.tuwien.me.xspetrinet.XSPetriNetMT",
    	xSPetriNetMT
    );
  }
}
