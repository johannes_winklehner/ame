package at.ac.tuwien.me.xspetrinet.xspetrinet.adapters.xspetrinetmt.petrinet;

import at.ac.tuwien.me.xspetrinet.xspetrinet.adapters.xspetrinetmt.XSPetriNetMTAdaptersFactory;
import at.ac.tuwien.me.xspetrinet.xspetrinet.petrinet.Place;
import at.ac.tuwien.me.xspetrinet.xspetrinetmt.petrinet.Transition;
import fr.inria.diverse.melange.adapters.EObjectAdapter;
import java.util.Collection;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;

@SuppressWarnings("all")
public class PlaceAdapter extends EObjectAdapter<Place> implements at.ac.tuwien.me.xspetrinet.xspetrinetmt.petrinet.Place {
  private XSPetriNetMTAdaptersFactory adaptersFactory;
  
  public PlaceAdapter() {
    super(at.ac.tuwien.me.xspetrinet.xspetrinet.adapters.xspetrinetmt.XSPetriNetMTAdaptersFactory.getInstance());
    adaptersFactory = at.ac.tuwien.me.xspetrinet.xspetrinet.adapters.xspetrinetmt.XSPetriNetMTAdaptersFactory.getInstance();
  }
  
  @Override
  public String getName() {
    return adaptee.getName();
  }
  
  @Override
  public void setName(final String o) {
    adaptee.setName(o);
  }
  
  @Override
  public int getTokens() {
    return adaptee.getTokens();
  }
  
  @Override
  public void setTokens(final int o) {
    adaptee.setTokens(o);
  }
  
  private EList<Transition> outgoingTransitions_;
  
  @Override
  public EList<Transition> getOutgoingTransitions() {
    if (outgoingTransitions_ == null)
    	outgoingTransitions_ = fr.inria.diverse.melange.adapters.EListAdapter.newInstance(adaptee.getOutgoingTransitions(), adaptersFactory, eResource);
    return outgoingTransitions_;
  }
  
  private EList<Transition> incomingTransitions_;
  
  @Override
  public EList<Transition> getIncomingTransitions() {
    if (incomingTransitions_ == null)
    	incomingTransitions_ = fr.inria.diverse.melange.adapters.EListAdapter.newInstance(adaptee.getIncomingTransitions(), adaptersFactory, eResource);
    return incomingTransitions_;
  }
  
  @Override
  public void addToken() {
    at.ac.tuwien.me.xspetrinet.xspetrinet.aspects.PlaceAspect.addToken(adaptee);
  }
  
  @Override
  public void takeToken() {
    at.ac.tuwien.me.xspetrinet.xspetrinet.aspects.PlaceAspect.takeToken(adaptee);
  }
  
  protected final static String NAME_EDEFAULT = null;
  
  protected final static int TOKENS_EDEFAULT = 0;
  
  @Override
  public EClass eClass() {
    return at.ac.tuwien.me.xspetrinet.xspetrinetmt.petrinet.PetrinetPackage.eINSTANCE.getPlace();
  }
  
  @Override
  public Object eGet(final int featureID, final boolean resolve, final boolean coreType) {
    switch (featureID) {
    	case at.ac.tuwien.me.xspetrinet.xspetrinetmt.petrinet.PetrinetPackage.PLACE__NAME:
    		return getName();
    	case at.ac.tuwien.me.xspetrinet.xspetrinetmt.petrinet.PetrinetPackage.PLACE__OUTGOING_TRANSITIONS:
    		return getOutgoingTransitions();
    	case at.ac.tuwien.me.xspetrinet.xspetrinetmt.petrinet.PetrinetPackage.PLACE__INCOMING_TRANSITIONS:
    		return getIncomingTransitions();
    	case at.ac.tuwien.me.xspetrinet.xspetrinetmt.petrinet.PetrinetPackage.PLACE__TOKENS:
    		return new java.lang.Integer(getTokens());
    }
    
    return super.eGet(featureID, resolve, coreType);
  }
  
  @Override
  public boolean eIsSet(final int featureID) {
    switch (featureID) {
    	case at.ac.tuwien.me.xspetrinet.xspetrinetmt.petrinet.PetrinetPackage.PLACE__NAME:
    		return getName() != NAME_EDEFAULT;
    	case at.ac.tuwien.me.xspetrinet.xspetrinetmt.petrinet.PetrinetPackage.PLACE__OUTGOING_TRANSITIONS:
    		return getOutgoingTransitions() != null && !getOutgoingTransitions().isEmpty();
    	case at.ac.tuwien.me.xspetrinet.xspetrinetmt.petrinet.PetrinetPackage.PLACE__INCOMING_TRANSITIONS:
    		return getIncomingTransitions() != null && !getIncomingTransitions().isEmpty();
    	case at.ac.tuwien.me.xspetrinet.xspetrinetmt.petrinet.PetrinetPackage.PLACE__TOKENS:
    		return getTokens() != TOKENS_EDEFAULT;
    }
    
    return super.eIsSet(featureID);
  }
  
  @Override
  public void eSet(final int featureID, final Object newValue) {
    switch (featureID) {
    	case at.ac.tuwien.me.xspetrinet.xspetrinetmt.petrinet.PetrinetPackage.PLACE__NAME:
    		setName(
    		(java.lang.String)
    		 newValue);
    		return;
    	case at.ac.tuwien.me.xspetrinet.xspetrinetmt.petrinet.PetrinetPackage.PLACE__OUTGOING_TRANSITIONS:
    		getOutgoingTransitions().clear();
    		getOutgoingTransitions().addAll((Collection) newValue);
    		return;
    	case at.ac.tuwien.me.xspetrinet.xspetrinetmt.petrinet.PetrinetPackage.PLACE__INCOMING_TRANSITIONS:
    		getIncomingTransitions().clear();
    		getIncomingTransitions().addAll((Collection) newValue);
    		return;
    	case at.ac.tuwien.me.xspetrinet.xspetrinetmt.petrinet.PetrinetPackage.PLACE__TOKENS:
    		setTokens(((java.lang.Integer) newValue).intValue());
    		return;
    }
    
    super.eSet(featureID, newValue);
  }
}
