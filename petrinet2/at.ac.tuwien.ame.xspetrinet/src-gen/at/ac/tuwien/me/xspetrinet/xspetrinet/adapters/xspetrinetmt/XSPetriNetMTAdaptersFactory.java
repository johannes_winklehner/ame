package at.ac.tuwien.me.xspetrinet.xspetrinet.adapters.xspetrinetmt;

import at.ac.tuwien.me.xspetrinet.xspetrinet.adapters.xspetrinetmt.petrinet.NamedElementAdapter;
import at.ac.tuwien.me.xspetrinet.xspetrinet.adapters.xspetrinetmt.petrinet.NetAdapter;
import at.ac.tuwien.me.xspetrinet.xspetrinet.adapters.xspetrinetmt.petrinet.PlaceAdapter;
import at.ac.tuwien.me.xspetrinet.xspetrinet.adapters.xspetrinetmt.petrinet.TransitionAdapter;
import at.ac.tuwien.me.xspetrinet.xspetrinet.petrinet.NamedElement;
import at.ac.tuwien.me.xspetrinet.xspetrinet.petrinet.Net;
import at.ac.tuwien.me.xspetrinet.xspetrinet.petrinet.Place;
import at.ac.tuwien.me.xspetrinet.xspetrinet.petrinet.Transition;
import fr.inria.diverse.melange.adapters.AdaptersFactory;
import fr.inria.diverse.melange.adapters.EObjectAdapter;
import java.util.WeakHashMap;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;

@SuppressWarnings("all")
public class XSPetriNetMTAdaptersFactory implements AdaptersFactory {
  private static XSPetriNetMTAdaptersFactory instance;
  
  public static XSPetriNetMTAdaptersFactory getInstance() {
    if (instance == null) {
    	instance = new at.ac.tuwien.me.xspetrinet.xspetrinet.adapters.xspetrinetmt.XSPetriNetMTAdaptersFactory();
    }
    return instance;
  }
  
  private WeakHashMap<EObject, EObjectAdapter> register;
  
  public XSPetriNetMTAdaptersFactory() {
    register = new WeakHashMap();
  }
  
  public EObjectAdapter createAdapter(final EObject o, final Resource res) {
    if (o instanceof at.ac.tuwien.me.xspetrinet.xspetrinet.petrinet.Net){
    	return createNetAdapter((at.ac.tuwien.me.xspetrinet.xspetrinet.petrinet.Net) o, res);
    }
    if (o instanceof at.ac.tuwien.me.xspetrinet.xspetrinet.petrinet.Place){
    	return createPlaceAdapter((at.ac.tuwien.me.xspetrinet.xspetrinet.petrinet.Place) o, res);
    }
    if (o instanceof at.ac.tuwien.me.xspetrinet.xspetrinet.petrinet.Transition){
    	return createTransitionAdapter((at.ac.tuwien.me.xspetrinet.xspetrinet.petrinet.Transition) o, res);
    }
    
    return null;
  }
  
  public NamedElementAdapter createNamedElementAdapter(final NamedElement adaptee, final Resource res) {
    if (adaptee == null)
    	return null;
    EObjectAdapter adapter = register.get(adaptee);
    if(adapter != null)
    	 return (at.ac.tuwien.me.xspetrinet.xspetrinet.adapters.xspetrinetmt.petrinet.NamedElementAdapter) adapter;
    else {
    	adapter = new at.ac.tuwien.me.xspetrinet.xspetrinet.adapters.xspetrinetmt.petrinet.NamedElementAdapter();
    	adapter.setAdaptee(adaptee);
    	adapter.setResource(res);
    	register.put(adaptee, adapter);
    	return (at.ac.tuwien.me.xspetrinet.xspetrinet.adapters.xspetrinetmt.petrinet.NamedElementAdapter) adapter;
    }
  }
  
  public NetAdapter createNetAdapter(final Net adaptee, final Resource res) {
    if (adaptee == null)
    	return null;
    EObjectAdapter adapter = register.get(adaptee);
    if(adapter != null)
    	 return (at.ac.tuwien.me.xspetrinet.xspetrinet.adapters.xspetrinetmt.petrinet.NetAdapter) adapter;
    else {
    	adapter = new at.ac.tuwien.me.xspetrinet.xspetrinet.adapters.xspetrinetmt.petrinet.NetAdapter();
    	adapter.setAdaptee(adaptee);
    	adapter.setResource(res);
    	register.put(adaptee, adapter);
    	return (at.ac.tuwien.me.xspetrinet.xspetrinet.adapters.xspetrinetmt.petrinet.NetAdapter) adapter;
    }
  }
  
  public PlaceAdapter createPlaceAdapter(final Place adaptee, final Resource res) {
    if (adaptee == null)
    	return null;
    EObjectAdapter adapter = register.get(adaptee);
    if(adapter != null)
    	 return (at.ac.tuwien.me.xspetrinet.xspetrinet.adapters.xspetrinetmt.petrinet.PlaceAdapter) adapter;
    else {
    	adapter = new at.ac.tuwien.me.xspetrinet.xspetrinet.adapters.xspetrinetmt.petrinet.PlaceAdapter();
    	adapter.setAdaptee(adaptee);
    	adapter.setResource(res);
    	register.put(adaptee, adapter);
    	return (at.ac.tuwien.me.xspetrinet.xspetrinet.adapters.xspetrinetmt.petrinet.PlaceAdapter) adapter;
    }
  }
  
  public TransitionAdapter createTransitionAdapter(final Transition adaptee, final Resource res) {
    if (adaptee == null)
    	return null;
    EObjectAdapter adapter = register.get(adaptee);
    if(adapter != null)
    	 return (at.ac.tuwien.me.xspetrinet.xspetrinet.adapters.xspetrinetmt.petrinet.TransitionAdapter) adapter;
    else {
    	adapter = new at.ac.tuwien.me.xspetrinet.xspetrinet.adapters.xspetrinetmt.petrinet.TransitionAdapter();
    	adapter.setAdaptee(adaptee);
    	adapter.setResource(res);
    	register.put(adaptee, adapter);
    	return (at.ac.tuwien.me.xspetrinet.xspetrinet.adapters.xspetrinetmt.petrinet.TransitionAdapter) adapter;
    }
  }
}
