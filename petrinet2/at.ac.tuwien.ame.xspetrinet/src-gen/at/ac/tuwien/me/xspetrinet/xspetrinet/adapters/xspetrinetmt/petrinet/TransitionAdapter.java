package at.ac.tuwien.me.xspetrinet.xspetrinet.adapters.xspetrinetmt.petrinet;

import at.ac.tuwien.me.xspetrinet.xspetrinet.adapters.xspetrinetmt.XSPetriNetMTAdaptersFactory;
import at.ac.tuwien.me.xspetrinet.xspetrinet.petrinet.Transition;
import at.ac.tuwien.me.xspetrinet.xspetrinetmt.petrinet.Place;
import fr.inria.diverse.melange.adapters.EObjectAdapter;
import java.util.Collection;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;

@SuppressWarnings("all")
public class TransitionAdapter extends EObjectAdapter<Transition> implements at.ac.tuwien.me.xspetrinet.xspetrinetmt.petrinet.Transition {
  private XSPetriNetMTAdaptersFactory adaptersFactory;
  
  public TransitionAdapter() {
    super(at.ac.tuwien.me.xspetrinet.xspetrinet.adapters.xspetrinetmt.XSPetriNetMTAdaptersFactory.getInstance());
    adaptersFactory = at.ac.tuwien.me.xspetrinet.xspetrinet.adapters.xspetrinetmt.XSPetriNetMTAdaptersFactory.getInstance();
  }
  
  @Override
  public String getName() {
    return adaptee.getName();
  }
  
  @Override
  public void setName(final String o) {
    adaptee.setName(o);
  }
  
  private EList<Place> sources_;
  
  @Override
  public EList<Place> getSources() {
    if (sources_ == null)
    	sources_ = fr.inria.diverse.melange.adapters.EListAdapter.newInstance(adaptee.getSources(), adaptersFactory, eResource);
    return sources_;
  }
  
  private EList<Place> targets_;
  
  @Override
  public EList<Place> getTargets() {
    if (targets_ == null)
    	targets_ = fr.inria.diverse.melange.adapters.EListAdapter.newInstance(adaptee.getTargets(), adaptersFactory, eResource);
    return targets_;
  }
  
  @Override
  public void fire() {
    at.ac.tuwien.me.xspetrinet.xspetrinet.aspects.TransitionAspect.fire(adaptee);
  }
  
  @Override
  public boolean isActive() {
    return at.ac.tuwien.me.xspetrinet.xspetrinet.aspects.TransitionAspect.isActive(adaptee);
  }
  
  protected final static String NAME_EDEFAULT = null;
  
  @Override
  public EClass eClass() {
    return at.ac.tuwien.me.xspetrinet.xspetrinetmt.petrinet.PetrinetPackage.eINSTANCE.getTransition();
  }
  
  @Override
  public Object eGet(final int featureID, final boolean resolve, final boolean coreType) {
    switch (featureID) {
    	case at.ac.tuwien.me.xspetrinet.xspetrinetmt.petrinet.PetrinetPackage.TRANSITION__NAME:
    		return getName();
    	case at.ac.tuwien.me.xspetrinet.xspetrinetmt.petrinet.PetrinetPackage.TRANSITION__SOURCES:
    		return getSources();
    	case at.ac.tuwien.me.xspetrinet.xspetrinetmt.petrinet.PetrinetPackage.TRANSITION__TARGETS:
    		return getTargets();
    }
    
    return super.eGet(featureID, resolve, coreType);
  }
  
  @Override
  public boolean eIsSet(final int featureID) {
    switch (featureID) {
    	case at.ac.tuwien.me.xspetrinet.xspetrinetmt.petrinet.PetrinetPackage.TRANSITION__NAME:
    		return getName() != NAME_EDEFAULT;
    	case at.ac.tuwien.me.xspetrinet.xspetrinetmt.petrinet.PetrinetPackage.TRANSITION__SOURCES:
    		return getSources() != null && !getSources().isEmpty();
    	case at.ac.tuwien.me.xspetrinet.xspetrinetmt.petrinet.PetrinetPackage.TRANSITION__TARGETS:
    		return getTargets() != null && !getTargets().isEmpty();
    }
    
    return super.eIsSet(featureID);
  }
  
  @Override
  public void eSet(final int featureID, final Object newValue) {
    switch (featureID) {
    	case at.ac.tuwien.me.xspetrinet.xspetrinetmt.petrinet.PetrinetPackage.TRANSITION__NAME:
    		setName(
    		(java.lang.String)
    		 newValue);
    		return;
    	case at.ac.tuwien.me.xspetrinet.xspetrinetmt.petrinet.PetrinetPackage.TRANSITION__SOURCES:
    		getSources().clear();
    		getSources().addAll((Collection) newValue);
    		return;
    	case at.ac.tuwien.me.xspetrinet.xspetrinetmt.petrinet.PetrinetPackage.TRANSITION__TARGETS:
    		getTargets().clear();
    		getTargets().addAll((Collection) newValue);
    		return;
    }
    
    super.eSet(featureID, newValue);
  }
}
