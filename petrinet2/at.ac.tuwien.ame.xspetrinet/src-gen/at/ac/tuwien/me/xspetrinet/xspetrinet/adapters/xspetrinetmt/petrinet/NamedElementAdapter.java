package at.ac.tuwien.me.xspetrinet.xspetrinet.adapters.xspetrinetmt.petrinet;

import at.ac.tuwien.me.xspetrinet.xspetrinet.adapters.xspetrinetmt.XSPetriNetMTAdaptersFactory;
import at.ac.tuwien.me.xspetrinet.xspetrinet.petrinet.NamedElement;
import fr.inria.diverse.melange.adapters.EObjectAdapter;
import org.eclipse.emf.ecore.EClass;

@SuppressWarnings("all")
public class NamedElementAdapter extends EObjectAdapter<NamedElement> implements at.ac.tuwien.me.xspetrinet.xspetrinetmt.petrinet.NamedElement {
  private XSPetriNetMTAdaptersFactory adaptersFactory;
  
  public NamedElementAdapter() {
    super(at.ac.tuwien.me.xspetrinet.xspetrinet.adapters.xspetrinetmt.XSPetriNetMTAdaptersFactory.getInstance());
    adaptersFactory = at.ac.tuwien.me.xspetrinet.xspetrinet.adapters.xspetrinetmt.XSPetriNetMTAdaptersFactory.getInstance();
  }
  
  @Override
  public String getName() {
    return adaptee.getName();
  }
  
  @Override
  public void setName(final String o) {
    adaptee.setName(o);
  }
  
  protected final static String NAME_EDEFAULT = null;
  
  @Override
  public EClass eClass() {
    return at.ac.tuwien.me.xspetrinet.xspetrinetmt.petrinet.PetrinetPackage.eINSTANCE.getNamedElement();
  }
  
  @Override
  public Object eGet(final int featureID, final boolean resolve, final boolean coreType) {
    switch (featureID) {
    	case at.ac.tuwien.me.xspetrinet.xspetrinetmt.petrinet.PetrinetPackage.NAMED_ELEMENT__NAME:
    		return getName();
    }
    
    return super.eGet(featureID, resolve, coreType);
  }
  
  @Override
  public boolean eIsSet(final int featureID) {
    switch (featureID) {
    	case at.ac.tuwien.me.xspetrinet.xspetrinetmt.petrinet.PetrinetPackage.NAMED_ELEMENT__NAME:
    		return getName() != NAME_EDEFAULT;
    }
    
    return super.eIsSet(featureID);
  }
  
  @Override
  public void eSet(final int featureID, final Object newValue) {
    switch (featureID) {
    	case at.ac.tuwien.me.xspetrinet.xspetrinetmt.petrinet.PetrinetPackage.NAMED_ELEMENT__NAME:
    		setName(
    		(java.lang.String)
    		 newValue);
    		return;
    }
    
    super.eSet(featureID, newValue);
  }
}
