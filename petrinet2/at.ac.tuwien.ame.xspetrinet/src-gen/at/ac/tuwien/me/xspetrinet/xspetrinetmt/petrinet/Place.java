/**
 */
package at.ac.tuwien.me.xspetrinet.xspetrinetmt.petrinet;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Place</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.me.xspetrinet.xspetrinetmt.petrinet.Place#getOutgoingTransitions <em>Outgoing Transitions</em>}</li>
 *   <li>{@link at.ac.tuwien.me.xspetrinet.xspetrinetmt.petrinet.Place#getIncomingTransitions <em>Incoming Transitions</em>}</li>
 *   <li>{@link at.ac.tuwien.me.xspetrinet.xspetrinetmt.petrinet.Place#getTokens <em>Tokens</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.me.xspetrinet.xspetrinetmt.petrinet.PetrinetPackage#getPlace()
 * @model
 * @generated
 */
public interface Place extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Outgoing Transitions</b></em>' reference list.
	 * The list contents are of type {@link at.ac.tuwien.me.xspetrinet.xspetrinetmt.petrinet.Transition}.
	 * It is bidirectional and its opposite is '{@link at.ac.tuwien.me.xspetrinet.xspetrinetmt.petrinet.Transition#getSources <em>Sources</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Outgoing Transitions</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Outgoing Transitions</em>' reference list.
	 * @see at.ac.tuwien.me.xspetrinet.xspetrinetmt.petrinet.PetrinetPackage#getPlace_OutgoingTransitions()
	 * @see at.ac.tuwien.me.xspetrinet.xspetrinetmt.petrinet.Transition#getSources
	 * @model opposite="sources"
	 * @generated
	 */
	EList<Transition> getOutgoingTransitions();

	/**
	 * Returns the value of the '<em><b>Incoming Transitions</b></em>' reference list.
	 * The list contents are of type {@link at.ac.tuwien.me.xspetrinet.xspetrinetmt.petrinet.Transition}.
	 * It is bidirectional and its opposite is '{@link at.ac.tuwien.me.xspetrinet.xspetrinetmt.petrinet.Transition#getTargets <em>Targets</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Incoming Transitions</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Incoming Transitions</em>' reference list.
	 * @see at.ac.tuwien.me.xspetrinet.xspetrinetmt.petrinet.PetrinetPackage#getPlace_IncomingTransitions()
	 * @see at.ac.tuwien.me.xspetrinet.xspetrinetmt.petrinet.Transition#getTargets
	 * @model opposite="targets"
	 * @generated
	 */
	EList<Transition> getIncomingTransitions();

	/**
	 * Returns the value of the '<em><b>Tokens</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tokens</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tokens</em>' attribute.
	 * @see #setTokens(int)
	 * @see at.ac.tuwien.me.xspetrinet.xspetrinetmt.petrinet.PetrinetPackage#getPlace_Tokens()
	 * @model default="0" required="true"
	 * @generated
	 */
	int getTokens();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.me.xspetrinet.xspetrinetmt.petrinet.Place#getTokens <em>Tokens</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Tokens</em>' attribute.
	 * @see #getTokens()
	 * @generated
	 */
	void setTokens(int value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void addToken();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void takeToken();

} // Place
