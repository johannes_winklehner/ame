package at.ac.tuwien.ame.petrinet.k3dsa;

import at.ac.tuwien.ame.petrinet.Transition;
import at.ac.tuwien.ame.petrinet.k3dsa.TransitionAspectTransitionAspectProperties;
import java.util.Map;

@SuppressWarnings("all")
public class TransitionAspectTransitionAspectContext {
  public final static TransitionAspectTransitionAspectContext INSTANCE = new TransitionAspectTransitionAspectContext();
  
  public static TransitionAspectTransitionAspectProperties getSelf(final Transition _self) {
    		if (!INSTANCE.map.containsKey(_self))
    			INSTANCE.map.put(_self, new at.ac.tuwien.ame.petrinet.k3dsa.TransitionAspectTransitionAspectProperties());
    		return INSTANCE.map.get(_self);
  }
  
  private Map<Transition, TransitionAspectTransitionAspectProperties> map = new java.util.WeakHashMap<at.ac.tuwien.ame.petrinet.Transition, at.ac.tuwien.ame.petrinet.k3dsa.TransitionAspectTransitionAspectProperties>();
  
  public Map<Transition, TransitionAspectTransitionAspectProperties> getMap() {
    return map;
  }
}
