package at.ac.tuwien.ame.petrinet.k3dsa;

import at.ac.tuwien.ame.petrinet.Net;
import at.ac.tuwien.ame.petrinet.Transition;
import at.ac.tuwien.ame.petrinet.k3dsa.NetAspectNetAspectProperties;
import at.ac.tuwien.ame.petrinet.k3dsa.TransitionAspect;
import com.google.common.base.Objects;
import fr.inria.diverse.k3.al.annotationprocessor.Aspect;
import fr.inria.diverse.k3.al.annotationprocessor.Main;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;

@Aspect(className = Net.class)
@SuppressWarnings("all")
public class NetAspect {
  @Main
  public static void main(final Net _self) {
    final at.ac.tuwien.ame.petrinet.k3dsa.NetAspectNetAspectProperties _self_ = at.ac.tuwien.ame.petrinet.k3dsa.NetAspectNetAspectContext.getSelf(_self);
    _privk3_main(_self_, _self);;
  }
  
  protected static void _privk3_main(final NetAspectNetAspectProperties _self_, final Net _self) {
    System.out.println("do something");
    for (Transition t = IterableExtensions.<Transition>findFirst(_self.getTransitions(), ((Function1<Transition, Boolean>) (Transition it) -> {
      return Boolean.valueOf(TransitionAspect.isActive(it));
    })); (!Objects.equal(t, null)); IterableExtensions.<Transition>findFirst(_self.getTransitions(), ((Function1<Transition, Boolean>) (Transition it) -> {
      return Boolean.valueOf(TransitionAspect.isActive(it));
    }))) {
      TransitionAspect.fire(t);
    }
  }
}
