package at.ac.tuwien.ame.petrinet.k3dsa;

import at.ac.tuwien.ame.petrinet.Place;
import at.ac.tuwien.ame.petrinet.Transition;
import at.ac.tuwien.ame.petrinet.k3dsa.PlaceAspect;
import at.ac.tuwien.ame.petrinet.k3dsa.TransitionAspectTransitionAspectProperties;
import fr.inria.diverse.k3.al.annotationprocessor.Aspect;
import fr.inria.diverse.k3.al.annotationprocessor.Step;
import java.util.function.Consumer;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;

@Aspect(className = Transition.class)
@SuppressWarnings("all")
public class TransitionAspect {
  @Step
  public static void fire(final Transition _self) {
    final at.ac.tuwien.ame.petrinet.k3dsa.TransitionAspectTransitionAspectProperties _self_ = at.ac.tuwien.ame.petrinet.k3dsa.TransitionAspectTransitionAspectContext.getSelf(_self);
    fr.inria.diverse.k3.al.annotationprocessor.stepmanager.StepCommand command = new fr.inria.diverse.k3.al.annotationprocessor.stepmanager.StepCommand() {
    	@Override
    	public void execute() {
    		_privk3_fire(_self_, _self);
    	}
    };
    fr.inria.diverse.k3.al.annotationprocessor.stepmanager.IStepManager stepManager = fr.inria.diverse.k3.al.annotationprocessor.stepmanager.StepManagerRegistry.getInstance().findStepManager(_self);
    if (stepManager != null) {
    	stepManager.executeStep(_self,command,"Transition","fire");
    } else {
    	fr.inria.diverse.k3.al.annotationprocessor.stepmanager.IEventManager eventManager = fr.inria.diverse.k3.al.annotationprocessor.stepmanager.EventManagerRegistry.getInstance().findEventManager(_self);
    	if (eventManager != null) {
    		eventManager.manageEvents();
    	}
    	command.execute();
    }
    ;;
  }
  
  public static boolean isActive(final Transition _self) {
    final at.ac.tuwien.ame.petrinet.k3dsa.TransitionAspectTransitionAspectProperties _self_ = at.ac.tuwien.ame.petrinet.k3dsa.TransitionAspectTransitionAspectContext.getSelf(_self);
    Object result = null;
    result = _privk3_isActive(_self_, _self);;
    return (boolean)result;
  }
  
  protected static void _privk3_fire(final TransitionAspectTransitionAspectProperties _self_, final Transition _self) {
    EList<Place> _sources = _self.getSources();
    final Consumer<Place> _function = (Place it) -> {
      PlaceAspect.addToken(it);
    };
    _sources.forEach(_function);
    EList<Place> _targets = _self.getTargets();
    final Consumer<Place> _function_1 = (Place it) -> {
      PlaceAspect.takeToken(it);
    };
    _targets.forEach(_function_1);
  }
  
  protected static boolean _privk3_isActive(final TransitionAspectTransitionAspectProperties _self_, final Transition _self) {
    EList<Place> _sources = _self.getSources();
    final Function1<Place, Boolean> _function = (Place it) -> {
      int _tokens = it.getTokens();
      return Boolean.valueOf((_tokens > 0));
    };
    return IterableExtensions.<Place>forall(_sources, _function);
  }
}
