package at.ac.tuwien.ame.petrinet.k3dsa;

import at.ac.tuwien.ame.petrinet.Place;
import at.ac.tuwien.ame.petrinet.k3dsa.PlaceAspectPlaceAspectProperties;
import java.util.Map;

@SuppressWarnings("all")
public class PlaceAspectPlaceAspectContext {
  public final static PlaceAspectPlaceAspectContext INSTANCE = new PlaceAspectPlaceAspectContext();
  
  public static PlaceAspectPlaceAspectProperties getSelf(final Place _self) {
    		if (!INSTANCE.map.containsKey(_self))
    			INSTANCE.map.put(_self, new at.ac.tuwien.ame.petrinet.k3dsa.PlaceAspectPlaceAspectProperties());
    		return INSTANCE.map.get(_self);
  }
  
  private Map<Place, PlaceAspectPlaceAspectProperties> map = new java.util.WeakHashMap<at.ac.tuwien.ame.petrinet.Place, at.ac.tuwien.ame.petrinet.k3dsa.PlaceAspectPlaceAspectProperties>();
  
  public Map<Place, PlaceAspectPlaceAspectProperties> getMap() {
    return map;
  }
}
