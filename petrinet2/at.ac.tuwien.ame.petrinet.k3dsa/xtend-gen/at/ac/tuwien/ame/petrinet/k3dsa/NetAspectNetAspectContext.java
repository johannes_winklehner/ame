package at.ac.tuwien.ame.petrinet.k3dsa;

import at.ac.tuwien.ame.petrinet.Net;
import at.ac.tuwien.ame.petrinet.k3dsa.NetAspectNetAspectProperties;
import java.util.Map;

@SuppressWarnings("all")
public class NetAspectNetAspectContext {
  public final static NetAspectNetAspectContext INSTANCE = new NetAspectNetAspectContext();
  
  public static NetAspectNetAspectProperties getSelf(final Net _self) {
    		if (!INSTANCE.map.containsKey(_self))
    			INSTANCE.map.put(_self, new at.ac.tuwien.ame.petrinet.k3dsa.NetAspectNetAspectProperties());
    		return INSTANCE.map.get(_self);
  }
  
  private Map<Net, NetAspectNetAspectProperties> map = new java.util.WeakHashMap<at.ac.tuwien.ame.petrinet.Net, at.ac.tuwien.ame.petrinet.k3dsa.NetAspectNetAspectProperties>();
  
  public Map<Net, NetAspectNetAspectProperties> getMap() {
    return map;
  }
}
