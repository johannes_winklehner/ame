package at.ac.tuwien.ame.petrinet.k3dsa

import fr.inria.diverse.k3.al.annotationprocessor.Aspect
import at.ac.tuwien.ame.petrinet.Net
import at.ac.tuwien.ame.petrinet.Place
import at.ac.tuwien.ame.petrinet.Transition

import static extension at.ac.tuwien.ame.petrinet.k3dsa.PlaceAspect.*
import static extension at.ac.tuwien.ame.petrinet.k3dsa.TransitionAspect.*
import fr.inria.diverse.k3.al.annotationprocessor.Main
import fr.inria.diverse.k3.al.annotationprocessor.Step

@Aspect(className=Net)
class NetAspect {

	@Main
	def public void main() {
		System.out.println("do something")
		for (var t = _self.transitions.findFirst[isActive]; t != null; _self.transitions.findFirst[isActive]) {
			t.fire
		}
	}
}

@Aspect(className=Place)
class PlaceAspect {

	def public void addToken() {
		_self.tokens = _self.tokens + 1
	}

	def public void takeToken() {
		_self.tokens = _self.tokens - 1
	}
}

@Aspect(className=Transition)
class TransitionAspect {

	@Step
	def public void fire() {
		_self.sources.forEach[addToken]
		_self.targets.forEach[takeToken]
	}

	def public boolean isActive() {
		return _self.sources.forall[tokens > 0]
	}
}
