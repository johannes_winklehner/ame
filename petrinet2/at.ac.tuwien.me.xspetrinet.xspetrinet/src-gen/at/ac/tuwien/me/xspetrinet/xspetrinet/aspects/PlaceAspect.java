package at.ac.tuwien.me.xspetrinet.xspetrinet.aspects;

import at.ac.tuwien.me.xspetrinet.xspetrinet.petrinet.Place;
import at.ac.tuwien.me.xspetrinet.xspetrinet.aspects.PlaceAspectPlaceAspectProperties;
import fr.inria.diverse.k3.al.annotationprocessor.Aspect;

@Aspect(className = Place.class)
@SuppressWarnings("all")
public class PlaceAspect {
  public static void addToken(final Place _self) {
	final at.ac.tuwien.me.xspetrinet.xspetrinet.aspects.PlaceAspectPlaceAspectProperties _self_ = at.ac.tuwien.me.xspetrinet.xspetrinet.aspects.PlaceAspectPlaceAspectContext
			.getSelf(_self);
	_privk3_addToken(_self_, _self);
	;
}
  
  public static void takeToken(final Place _self) {
	final at.ac.tuwien.me.xspetrinet.xspetrinet.aspects.PlaceAspectPlaceAspectProperties _self_ = at.ac.tuwien.me.xspetrinet.xspetrinet.aspects.PlaceAspectPlaceAspectContext
			.getSelf(_self);
	_privk3_takeToken(_self_, _self);
	;
}
  
  protected static void _privk3_addToken(final PlaceAspectPlaceAspectProperties _self_, final Place _self) {
    int _tokens = _self.getTokens();
    int _plus = (_tokens + 1);
    _self.setTokens(_plus);
  }
  
  protected static void _privk3_takeToken(final PlaceAspectPlaceAspectProperties _self_, final Place _self) {
    int _tokens = _self.getTokens();
    int _minus = (_tokens - 1);
    _self.setTokens(_minus);
  }
}
