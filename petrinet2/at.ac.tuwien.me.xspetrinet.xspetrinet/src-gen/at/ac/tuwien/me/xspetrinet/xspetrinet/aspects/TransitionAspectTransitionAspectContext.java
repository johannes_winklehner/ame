package at.ac.tuwien.me.xspetrinet.xspetrinet.aspects;

import at.ac.tuwien.me.xspetrinet.xspetrinet.petrinet.Transition;
import at.ac.tuwien.me.xspetrinet.xspetrinet.aspects.TransitionAspectTransitionAspectProperties;
import java.util.Map;

@SuppressWarnings("all")
public class TransitionAspectTransitionAspectContext {
  public final static TransitionAspectTransitionAspectContext INSTANCE = new TransitionAspectTransitionAspectContext();
  
  public static TransitionAspectTransitionAspectProperties getSelf(final Transition _self) {
    		if (!INSTANCE.map.containsKey(_self))
    			INSTANCE.map.put(_self, new at.ac.tuwien.me.xspetrinet.xspetrinet.aspects.TransitionAspectTransitionAspectProperties());
    		return INSTANCE.map.get(_self);
  }
  
  private Map<Transition, TransitionAspectTransitionAspectProperties> map = new java.util.WeakHashMap<at.ac.tuwien.me.xspetrinet.xspetrinet.petrinet.Transition, at.ac.tuwien.me.xspetrinet.xspetrinet.aspects.TransitionAspectTransitionAspectProperties>();
  
  public Map<Transition, TransitionAspectTransitionAspectProperties> getMap() {
    return map;
  }
}
