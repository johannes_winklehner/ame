package at.ac.tuwien.me.xspetrinet.xspetrinet.aspects;

import at.ac.tuwien.me.xspetrinet.xspetrinet.petrinet.Place;
import at.ac.tuwien.me.xspetrinet.xspetrinet.aspects.PlaceAspectPlaceAspectProperties;
import java.util.Map;

@SuppressWarnings("all")
public class PlaceAspectPlaceAspectContext {
  public final static PlaceAspectPlaceAspectContext INSTANCE = new PlaceAspectPlaceAspectContext();
  
  public static PlaceAspectPlaceAspectProperties getSelf(final Place _self) {
    		if (!INSTANCE.map.containsKey(_self))
    			INSTANCE.map.put(_self, new at.ac.tuwien.me.xspetrinet.xspetrinet.aspects.PlaceAspectPlaceAspectProperties());
    		return INSTANCE.map.get(_self);
  }
  
  private Map<Place, PlaceAspectPlaceAspectProperties> map = new java.util.WeakHashMap<at.ac.tuwien.me.xspetrinet.xspetrinet.petrinet.Place, at.ac.tuwien.me.xspetrinet.xspetrinet.aspects.PlaceAspectPlaceAspectProperties>();
  
  public Map<Place, PlaceAspectPlaceAspectProperties> getMap() {
    return map;
  }
}
