package at.ac.tuwien.me.xspetrinet.xspetrinet.aspects;

import at.ac.tuwien.me.xspetrinet.xspetrinet.petrinet.Net;
import at.ac.tuwien.me.xspetrinet.xspetrinet.aspects.NetAspectNetAspectProperties;
import java.util.Map;

@SuppressWarnings("all")
public class NetAspectNetAspectContext {
  public final static NetAspectNetAspectContext INSTANCE = new NetAspectNetAspectContext();
  
  public static NetAspectNetAspectProperties getSelf(final Net _self) {
    		if (!INSTANCE.map.containsKey(_self))
    			INSTANCE.map.put(_self, new at.ac.tuwien.me.xspetrinet.xspetrinet.aspects.NetAspectNetAspectProperties());
    		return INSTANCE.map.get(_self);
  }
  
  private Map<Net, NetAspectNetAspectProperties> map = new java.util.WeakHashMap<at.ac.tuwien.me.xspetrinet.xspetrinet.petrinet.Net, at.ac.tuwien.me.xspetrinet.xspetrinet.aspects.NetAspectNetAspectProperties>();
  
  public Map<Net, NetAspectNetAspectProperties> getMap() {
    return map;
  }
}
