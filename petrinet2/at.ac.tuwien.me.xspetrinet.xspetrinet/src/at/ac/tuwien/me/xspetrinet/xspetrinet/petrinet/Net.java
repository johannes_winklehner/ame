/**
 */
package at.ac.tuwien.me.xspetrinet.xspetrinet.petrinet;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Net</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.me.xspetrinet.xspetrinet.petrinet.Net#getPlaces <em>Places</em>}</li>
 *   <li>{@link at.ac.tuwien.me.xspetrinet.xspetrinet.petrinet.Net#getTransitions <em>Transitions</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.me.xspetrinet.xspetrinet.petrinet.PetrinetPackage#getNet()
 * @model
 * @generated
 */
public interface Net extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Places</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.me.xspetrinet.xspetrinet.petrinet.Place}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Places</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Places</em>' containment reference list.
	 * @see at.ac.tuwien.me.xspetrinet.xspetrinet.petrinet.PetrinetPackage#getNet_Places()
	 * @model containment="true"
	 * @generated
	 */
	EList<Place> getPlaces();

	/**
	 * Returns the value of the '<em><b>Transitions</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.me.xspetrinet.xspetrinet.petrinet.Transition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Transitions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Transitions</em>' containment reference list.
	 * @see at.ac.tuwien.me.xspetrinet.xspetrinet.petrinet.PetrinetPackage#getNet_Transitions()
	 * @model containment="true"
	 * @generated
	 */
	EList<Transition> getTransitions();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void main();

} // Net
