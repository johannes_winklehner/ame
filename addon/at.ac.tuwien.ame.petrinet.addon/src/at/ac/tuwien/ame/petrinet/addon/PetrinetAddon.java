package at.ac.tuwien.ame.petrinet.addon;

import java.io.IOException;

import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.IConsoleManager;
import org.eclipse.ui.console.IOConsoleOutputStream;
import org.eclipse.ui.console.MessageConsole;
import org.gemoc.commons.eclipse.emf.EMFResource;
import org.gemoc.xdsmlframework.api.core.IExecutionEngine;
import org.gemoc.xdsmlframework.api.engine_addon.DefaultEngineAddon;
import org.gemoc.xdsmlframework.api.engine_addon.modelchangelistener.BatchModelChangeListener;

import fr.inria.diverse.trace.commons.model.trace.Step;

public class PetrinetAddon extends DefaultEngineAddon {

	private BatchModelChangeListener listener;

	private MessageConsole findConsole(String name) {
		ConsolePlugin plugin = ConsolePlugin.getDefault();
		IConsoleManager conMan = plugin.getConsoleManager();
		IConsole[] existing = conMan.getConsoles();
		for (int i = 0; i < existing.length; i++)
			if (name.equals(existing[i].getName()))
				return (MessageConsole) existing[i];
		// no console found, so create a new one
		MessageConsole myConsole = new MessageConsole(name, null);
		conMan.addConsoles(new IConsole[] { myConsole });
		return myConsole;
	}

	@Override
	public void aboutToExecuteStep(IExecutionEngine executionEngine, Step<?> stepToApply) {
		MessageConsole console = findConsole("testplugin");
		console.activate();
		IOConsoleOutputStream stream = console.newOutputStream();
		try {
			listener.getChanges(this).forEach(c -> {
				try {
					stream.write(c.getChangedObject()+"\n");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			});
			stream.write("\n\n");
			stream.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// TODO Auto-generated method stub
		super.aboutToExecuteStep(executionEngine, stepToApply);
	}
	
	@Override
	public void engineAboutToStart(IExecutionEngine engine) {
		// TODO Auto-generated method stub
		super.engineAboutToStart(engine);
		
		listener = new BatchModelChangeListener(EMFResource.getRelatedResources(engine.getExecutionContext().getResourceModel()));
		listener.registerObserver(this);
	}
}
